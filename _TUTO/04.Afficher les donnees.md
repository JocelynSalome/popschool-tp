# Afficher les données de la bibliothèque

> __Prérequis :__ [Routes et controllers](_TUTO/03.Routes et controllers.md)    
> __Objectif  :__ Comprendre le fonctionnement asynchrone et le langage de template Pug et comment récupérer les données des URLs.

## Résumé

Nous avons créer :
- nos modèles Mongoose qui interagissent avec la BDD
- les routes requises pour notre site internet
- des controllers vides avec des "NOT IMPLEMENTED"

La prochaine étape est d'implémenter les fonctions de nos controllers pour afficher les infos.
Cela implique de mettre à jour nos controllers pour aller chercher les enregistrements dans la base de données et créer des templates pour les afficher.

## Flux asynchone

Le code des controllers pour certaines de nos pages va dépendre du résultats de plusieurs requêtes asynchrone, qui peuvent être exécuter dans un ordre particulier ou en parallèle.

Nous allons utiliser les [Promises](https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Utiliser_les_promesses).

### Pourquoi est-ce nécessaire ?

La plupart des méthodes que l'on utilise dans Express sont asynchrones : vous spécifiez une opération à être exécuter et passez un callback.
La méthode retourne un résultat tout de suite et le callback est invoqué quand l'opération est vraiment terminée.

Si un controller ne doit effectuer qu'une seule opération asynchone pour récupérer des informations et les afficher en HTML, c'est alors facile.
Un exemple avec une fonction qui fait le rendu d'un compte de ligne (avec la méthode Mongoose `count()`) dans la base de données :

```js
exports.some_model_count = function(req, res) {
 
  SomeModel.count({ a_model_field: 'match_value' }, function (err, count) {
    // ... on fait quelque chose s'il y a une erreur

    // En cas de succès, on fait le rendu du résultat en lui passant le compte (variable: data).
    res.render('the_template', { data: count } );
  });
}
```

Par contre, si vous devez avoir plusieurs appels asynchrone, on peut vite arriver dans une grande série de callbacks ([callback hell !](http://callbackhell.com/)) :

```js
obj.func(function(err, data1) {
    obj.abc(function(err, data2) {
        obj.xyz(function(err, data3) {
            obj.def(function(err, data4) {
                ...
            });                 
        });
    });
    return true;
})
```

### Opérations asynchrones en parallèle

La méthode [`Promise.all()`](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Promise/all) est utilisée pour exécuter plusieurs opérations asynchrones en parallèle.

Elle prend en paramètre un tableau des promesses à exécuter.

On utilse la méthode `then()` pour récupérer le résultat. Elle peut prendre jusqu'à deux arguments qui sont deux fonctions callback à utiliser en cas de succès ou d'échec de la Promise.

Exemple : 
```js
var p1 = Promise.resolve(3);
var p2 = 1337;
var p3 = new Promise((resolve, reject) => {
  setTimeout(resolve, 100, 'test');
}); 

Promise.all([p1, p2, p3]).then(values => { 
  console.log(values); // [3, 1337, "test"] 
});
```

### Opérations asynchrones en série

```js
Promise.resolve("toto")
  // 1. Première étape, on reçoit "toto" et on le concatène avec
  //    "truc", ce qui résoud la première étape puis on passe au
  //    deuxième then
  .then(function(string) {
    return new Promise(function(resolve, reject) {
      setTimeout(function() {
        string += 'truc';
        resolve(string);
      }, 1);
    });
  })
  // 2. Deuxième étape, on reçoit "tototruc" et on enregistre une
  //    fonction de rappel pour manipuler cette chaîne puis l'imprimer
  //    dans la console. Avant cela, on passe la chaîne intacte au
  //    prochain then
  .then(function(string) {
    setTimeout(function() {
      string += 'baz';
      console.log(string);
    }, 1)
    return string;
  })
  // 3. On affiche un message sur le code, celui-ci sera affiché
  //    avant que la chaîne soit traitée dans le bloc précédent
  //    qui agit comme un bloc asynchrone.
  .then(function(string) {
    console.log("Et voilà la dernière, qui risque d'arriver avant la 2e");

    // Ici, la chaîne n'aura pas le morceau 'baz' car la fonction
    // setTimeout retarde l'exécution du code.
    console.log(string);
});
```

### Opérations asynchrones dépendantes les unes des autres en série

Même exemple :

```js
Promise.resolve("toto")
  // 1. Première étape, on reçoit "toto" et on le concatène avec
  //    "truc", ce qui résoud la première étape puis on passe au
  //    deuxième then
  .then(function(string) {
    return new Promise(function(resolve, reject) {
      setTimeout(function() {
        string += 'truc';
        resolve(string);
      }, 1);
    });
  })
  // 2. Deuxième étape, on reçoit "tototruc" et on le concatène avec
  //    "baz", ce qui résoud la deuxième étape puis on passe au
  //    troisième then
  .then(function(string) {
    return new Promise(function(resolve, reject) {
      setTimeout(function() {
        string += 'baz';
        resolve(string);
      }, 1)
    });
  })
  // 3. On affiche un message sur le code, celui-ci sera affiché
  //    après que la chaîne soit traitée dans le bloc précédent.
  .then(function(string) {
    // Ici, la chaîne **aura** pas le morceau 'baz'.
    console.log(string);
});
```

## Template Pug

### Configuration

Notre appli est configurée pour utiliser Pug. Vous avez du voir que le module pug est inclus dans les dépendances dans le fichier `package.json` et que la configuration suivante est présente dans le fichier `app.js :

```js
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
```

Si vous allez voir dans le dossier `/views` vous verrez les fichier .pug.
Cela inclut la vue pour la page d'accueil (`index.pug`) et le template de base (`layout.pug`) que nous devrons remplacé avec notre propre contenu.

```bash
/popschool-tp
  /views
    error.pug
    index.pug
    layout.pug
    popschool.pug
```

### Syntaxe des templates

L'exemple ci-dessous présente les fonctionnalités les plus intéressantes de Pug.

La première a noter est que le fichier mappe la structure typique d'un fichier HTML, avec le premier mot de la première ligne.
Une indentation est utilisée pour indiquer les éléments imbriqués. Par exemple, l'élément `body` est dans l'élément `html` et les éléments paragraphe `p` sont dans l'élément `body`.
Les éléments non imbriqués sont sur une ligne séparée.


```pug
doctype html
html(lang="fr")
  head
    title= titre
    script(type='text/javascript').
  body
    h1= titre

    p C'est une ligne avec #[em une mise en avant em] et #[strong une text en gras].
    p Cette ligne a des données non échappées: !{'<em>est une mise en avant em</em>'} et des données échappées : #{'<em> pas une mise en avant em</em>'}. 
      | Cette ligne suit directement l'autre.
    p= 'Expression évaluée et <em>échappée</em>:' + titre

    <!-- On peut ajouter des commentaires HTML directement -->
    // Vous pouvez ajouter des commentaires Javascript et ils sont générés en commentaires HTML.
    //- Mettre un "//-" pour le commentaire permet de ne pas le sortir en HTML. 
    
    p Une ligne avec un lien 
      a(href='/catalog/authors') Texte du lien
      |  avec du texte en plus.
    
    #container.col
      if titre
        p Une variable "titre" existe.
      else
        p La variable "titre" n'existe pas.
      p.
        Pug est un langage très succinct et simple avec un focus sur les
        performances strong focus on performance and powerful features.
        
    h2 Generate a list
        
    ul
      each val in [1, 2, 3, 4, 5]
        li= val
```

Les attributs des éléments sont entre parenthèses après l'élément associé. A l'intérieur des paranthèses, les attributs sont définis par une liste de paires nom=valeur séparées par des virgules ou des espaces :
- script(type='text/javascript'), link(rel='stylesheet', href='/stylesheets/style.css')
- meta(name='viewport' content='width=device-width initial-scale=1')

Les valeurs de tous les attributes sont échappées, les caractères comme ">" sont convertis dans leur équivalent HTML ("&gt;") pour éviter les attaques croos-site scripting.

Si une balise est suivie avec le signe égale, le texte est traité comme une expression Javascript.
Par exemple, sur la première ligne le contenu de la balise `h1` sera la variable `titre` (définie dans le fichier ou passée en paramètre par Express)
Dans la seconde ligne, le contenu du paragraphe est un texte concaténée avec la variable `titre`.
```pug
h1= titre
p= 'Expression évaluée et <em>échappée</em>:' + titre
``` 

S'il n'y a pas de symbole égale après la balise, le contenu est traité comme du texte normal.
On peut insérer des données échappées et non échappées comme dans l'exemple ci-dessous :
```pug
p C'est une ligne avec #[em une mise en avant em] et #[strong une text en gras].
    p Cette ligne a des données non échappées: !{'<em>est une mise en avant em</em>'} et des données échappées : #{'<em> pas une mise en avant em</em>'}.
```

On peut utiliser le symbole pipe (`|`) au début d'une ligne pour indiquer du texte normal.
Par exemple, le texte additionnel affiché en dessous sera affiché sur la même ligne que le lien mais pas dans le lien :
```pug
a(href='/catalog/authors') Texte du lien
|  avec du texte en plus.
```

Pug permet aussi de créer des conditions avec `if`, `else`, `else if` et `unless` :
```pug
if titre
  p Une variable "titre" existe.
else
  p La variable "titre" n'existe pas.
```

On peut aussi créer des boucles/itérations avec `each-in` et `while` :
```pug
ul
  each val in [1, 2, 3, 4, 5]
    li= val
```

### Étendre les templates

Il est classique que les pages d'un site garde la même structure, incluant les standards HTML, les entêtes, le pied de page et la navigation, ...
Au lieu de forcer les développeurs à dupliquer le code sur toutes les pages, Pug permet de déclarer un template de base et de l'étendre en remplacant seulement les changements sur chaque page.

Par exemple, le template de base `layout.pug` créé dans le squelette ressemble à ça :
```pug
doctype html
html
  head
    title= title
    link(rel='stylesheet', href='/stylesheets/style.css')
  body
    block content
```

La balise `block` est utilisé pour démarrer une section de contenu qui peut être remplacée dans un template dérivé.   
Si le block n'est pas repris dans le template dérivé c'est celui de base qui est gardé.

Le template par défaut `index.pug` créé dans le squelette montre comment faire :
```pug
extends layout

block content
  h1= title
  p Welcome to #{title}
```

## Template de base pour MaBiliDeQuartier

Maintenant que vous en savez plus sur Pug, créons notre template de base.   
Il aura une sidebar avec les liens vers les pages et une section principale de contenu surchargée dans les pages.

Dans le fichier `/views/layout.pug` :

```pug
doctype html
html(lang='fr')
  head
    title= title
    meta(charset='utf-8')
    meta(name='viewport', content='width=device-width, initial-scale=1')
    link(rel='stylesheet', href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css')
    link(rel='stylesheet', href='/stylesheets/style.css')
    script(src='https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js')
    script(src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js')
  body
    div(class='container-fluid')
      div(class='row')
        div(class='col-sm-2')
          block sidebar
            ul(class='sidebar-nav')
              li 
                a(href='/catalog') Accueil
              li 
                a(href='/catalog/books') Les livres
              li 
                a(href='/catalog/authors') Les auteurs
              li 
                a(href='/catalog/genres') Les genres
              li 
                a(href='/catalog/bookinstances') Les exemplaires
              li 
                hr
              li 
                a(href='/catalog/author/create') Créer un auteur
              li 
                a(href='/catalog/genre/create') Créer un genre
              li 
                a(href='/catalog/book/create') Créer un livre
              li 
                a(href='/catalog/bookinstance/create') Créer un exemplaire
                
        div(class='col-sm-10')
          block content
```

Ce template utilise le JS et le CSS de [Bootstrap](https://getbootstrap.com/docs/3.3/) pour l'organisation des pages.
Le `block content` permet de définir une zone pour le contenu des pages. 

Pour les styles dans `/public/stylesheets/style.css` :
```css
.sidebar-nav {
    margin-top: 20px;
    padding: 0;
    list-style: none;
}
```

> Commit GIT : 6697ab94ce2efb1f8aae7c0f70cc48213a6a6f73

## Page d'accueil

La première page que nous allons créé est la page d'accueil du site accessible sur `/` ou `/catalog/`.

### Route

Nous avons déclaré la route dans `/routes/catalog.js` et le callback (`book_controller.index`) dans `/controllers/bookController.js` :
```js
exports.index = function(req, res, next) {   
    res.send('NOT IMPLEMENTED: Site Home Page');
}
``` 

### Controller

Le controller index doit aller chercher les infos sur le nombre d'enregistrements `Book`, `BookInstance` disponibles, `Author`, `Genre`.   
Nous utiliserons la méthode [`countDocuments()`](http://mongoosejs.com/docs/api.html#model_Model.countDocuments) pour récupérer le nombre de chaque instance de chaque modèle.

Dans `/controllers/bookController.js` : 

Remplacer le code ci-dessous
```js
var Book = require('../models/book');

exports.index = function(req, res) {
    res.send('NOT IMPLEMENTED: Site Home Page');
};
```

par :

```js
var Book = require('../models/book');
var Author = require('../models/author');
var Genre = require('../models/genre');
var BookInstance = require('../models/bookinstance');

exports.index = function(req, res) {

    Promise.all([
        new Promise(function(resolve, reject) { // book_count
            Book.countDocuments({}, function(err, count) {  // On passe un objet vide comme conditions de recherche pour tout récupérer
                if (err) {
                    reject(err);
                }
                resolve(count);
            });
        }),
        new Promise(function(resolve, reject) { // book_instance_count
            BookInstance.countDocuments({}, function(err, count) {
                if (err) {
                    reject(err);
                }
                resolve(count);
            });
        }),
        new Promise(function(resolve, reject) { // book_instance_available_count
            BookInstance.countDocuments({status: 'Available'}, function(err, count) {
                if (err) {
                    reject(err);
                }
                resolve(count);
            });
        }),
        new Promise(function(resolve, reject) { // author_count
            Author.countDocuments({}, function(err, count) {
                if (err) {
                    reject(err);
                }
                resolve(count);
            });
        }),
        new Promise(function(resolve, reject) { // genre_count
            Genre.countDocuments({}, function(err, count) {
                if (err) {
                    reject(err);
                }
                resolve(count);
            });
        }),
    ]).then(function(results) {
        console.log(results);
        res.render('index', { 
            title: 'Ma bibliothèque de quartier | Accueil', 
            data: {
                book_count: results[0],
                book_instance_count: results[1],
                book_instance_available_count: results[2],
                author_count: results[3],
                genre_count: results[4]
            }
        });
    }).catch(function(err) {
        res.render('error', { title: 'Ma bibliothèque de quartier | Accueil', error: err });
    });

};
```

On passe à la méthode `Promise.all()` un tableau avec des promsses pour récupérer les nombres d'enregistrements pour chaque modèle.
Ces fonctions sont toutes démarrées au même moment. Quand toutes sont terminées, le `then()` est invoqué avec les nomnbres ou `catch()` si une erreur est survenue.

En cas de succès, on appel `res.render()` pour spécifiant une vue nommée `index` et un objet contenant les données.

### Vue

Dans le fichier `/views/index.pug` et remplacer son contenu par : 

```pug
extends layout

block content
  h1= title
  p Bienvenue à #[em MaBiliDeQuartier], un site internet très simple développé avec Express comme TP Node chez Popschool.

  h1 Contenu dynamique

  p La bibliothèque a les enregistrements suivants :
    
  ul
    li #[strong Livres :] !{data.book_count}
    li #[strong Exemplaires :] !{data.book_instance_count}
    li #[strong Exemplaires disponibles :] !{data.book_instance_available_count} 
    li #[strong Auteurs :] !{data.author_count}
    li #[strong Genres :] !{data.genre_count}
```

> Commit GIT : c7990c0068d087fcf4eafdb071afdb4d8053d57c

### A quoi cela ressemble ?

![Chrome](/_TUTO/images/tuto4.jpeg)

## Page de liste des livres

### Controller

Dans le fichier `/controllers/bookController.js`, cherchez la méthode `book_list()` et remplacez la par :
```js
// Affiche la liste de tous les livres.
exports.book_list = function(req, res, next) {

  Book.find({}, 'title author')
    .populate('author')
    .exec(function (err, list_books) {
      if (err) { return next(err); }
      // Succès, on effectue le rendu
      res.render('book_list', { title: 'Liste des livres', book_list: list_books });
    });
    
};
```

On utilise la méthode `find()` pour retourner tous les livres en sélectionnant seulement le `title` et l'`author`.

### Vue

Créer le fichier `/views/book_list.pug` avec :
```pug
extends layout

block content
  h1= title
  
  ul
    each book in book_list
      li 
        a(href=book.url) #{book.title} 
        |  (#{book.author.name})

    else
      li Il n'y a aucun livre.
```

On utilise `book.url` pour avoir le lien vers le détail de l'enregistrement pour chaque livre. C'est une propriété virtuelle créée dans le modèle `Book`.

> Commit GIT : c1e05e545b8f5827d761076856d1a7526fc27896

### A quoi cela ressemble ?

![Chrome](/_TUTO/images/tuto5.jpeg)

## Page de liste des exemplaires

### Controller

Dans le fichier `/controllers/bookController.js`, cherchez la méthode `book_list()` et remplacez la par :
```js
// Affiche la liste de tous les exemplaires.
exports.bookinstance_list = function(req, res, next) {

    BookInstance.find()
        .populate('book')
        .exec(function (err, list_bookinstances) {
            if (err) { return next(err); }
            // Succès, on effectue le rendu
            res.render('bookinstance_list', { title: 'Liste des exemplaires', bookinstance_list: list_bookinstances });
        });

};
```

### Vue

Créer le fichier `/views/bookinstance_list.pug` avec :
```pug
extends layout

block content
  h1= title

  ul
    each val in bookinstance_list
      li 
        a(href=val.url) #{val.book.title} : #{val.imprint} - 
        if val.status=='Available'
          span.text-success Disponible
        else if val.status=='Maintenance'
          span.text-danger #{val.status}
        else if val.status=='Loaned'
          span.text-warning Emprunté
        else
          span.text-warning #{val.status}
        if val.status!='Available'
          span  (Due: #{val.due_back} )

    else
      li Il n'y a aucun exemplaire.
```

> Commit GIT : 162006c1bf159fa15f7e21cac2069f2b18ccbcac

### A quoi cela ressemble ?

![Chrome](/_TUTO/images/tuto6.jpeg)

## C'est à vous

Créer les pages :
- Liste des auteurs
- Liste des genres

> Commit GIT : 897878efeeaa4f18f9e0538b352d9fb4a2399eb1

## Page de détail d'un genre

### Controller

Dans `/controllers/genreController.js`.

On importe le modèle `Book` en haut du fichier :
```js
var Book = require('../models/book');
```

On implémente la méthode `genre_detail` :

```js
// Affiche la page de détail pour un genre.
exports.genre_detail = function(req, res, next) {

    Promise.all([
        new Promise(function(resolve, reject) { // genre
            Genre.findById(req.params.id).exec(function(err, genre) {
                if (err) {
                    reject(err);
                }
                resolve(genre);
            });
        }),
        new Promise(function(resolve, reject) { // genre_books
            Book.find({ 'genre': req.params.id }).exec(function(err, genre_books) {
                if (err) {
                    reject(err);
                }
                resolve(genre_books);
            });
        })
    ]).then(function(results) {
        
        if (results[0] == null) { // Pas de résultat => Erreur 404
            var err = new Error('Genre introuvable');
            err.status = 404;
            return next(err);
        }

        res.render('genre_detail', {
            title: "Détail d'un genre", 
            genre: results[0], 
            genre_books: results[1] 
        });

    }).catch(function(err) {
        next(err);
    });

};
```

### Vue

Créer le fichier `/views/genre_detail.pug` :

```pug
extends layout

block content

  h1 Genre : #{genre.name}
  
  div(style='margin-left:20px;margin-top:20px')

    h4 Livres
    
    dl
      each book in genre_books
        dt 
          a(href=book.url) #{book.title}
        dd #{book.summary}

      else
        p Ce genre n'a pas de livre
```

> Commit GIT : a25917d5719c452ab0d4bfff6a8e5affba6cbc86

### A quoi ça ressemble ?

![Chrome](/_TUTO/images/tuto7.jpeg)

## Page de détail d'un livre

### Controller

Dans `/controllers/bookController.js`.

```js
// Affiche la page de détail pour un livre.
exports.book_detail = function(req, res, next) {

    Promise.all([
        new Promise(function(resolve, reject) { // book
            Book.findById(req.params.id)
              .populate('author')
              .populate('genre')
              .exec(function(err, book) {
                if (err) {
                    reject(err);
                }
                resolve(book);
            });
        }),
        new Promise(function(resolve, reject) { // book_instance
            BookInstance.find({ 'book': req.params.id }).exec(function(err, book_instance) {
                if (err) {
                    reject(err);
                }
                resolve(book_instance);
            });
        })
    ]).then(function(results) {

        if (results[0] == null) { // Pas de résultat => Erreur 404
            var err = new Error('Livre introuvable');
            err.status = 404;
            return next(err);
        }

        res.render('book_detail', {
            title: "Détail d'un livre",
            book: results[0],
            book_instances: results[1]
        });

    }).catch(function(err) {
        next(err);
    });

};
```

### Vue

Créer le fichier `/views/book_detail.pug` :

```pug
extends layout

block content
  h1 #{title}: #{book.title}
  
  p #[strong Auteur :] 
    a(href=book.author.url) #{book.author.name}
  p #[strong Résumé :] #{book.summary}
  p #[strong ISBN :] #{book.isbn}
  p #[strong Genre :]&nbsp;
    each val, index in book.genre
      a(href=val.url) #{val.name}
      if index < book.genre.length - 1
        |, 
  
  div(style='margin-left:20px;margin-top:20px')
    h4 Exemplaires
    
    each val in book_instances
      hr
      if val.status=='Available'
        p.text-success Disponible
      else if val.status=='Maintenance'
        p.text-danger Maintenance
      else if val.status=='Loaned'
        p.text-warning Emprunté
      else
        p.text-warning #{val.status} 
      p #[strong Imprint:] #{val.imprint}
      if val.status!='Available'
        p #[strong Retour :] #{val.due_back}
      p #[strong Id:]&nbsp;
        a(href=val.url) #{val._id}
 
    else
      p Il n'y a aucun exemplaire de ce livre.
```

> Commit GIT : 34ee07b90bfe975e7c404427f11dd3c032a4a673

### A quoi ça ressemble ?

![Chrome](/_TUTO/images/tuto8.jpeg)

## C'est à vous

Créer les pages :
- Page de détail d'un auteur avec ses livres
- Page de détail d'un exemplaire avec le nom du livre et un lien vers sa page de détail

> Commit GIT : 34456fe421bb009cee606bae497ddda0c85560cb